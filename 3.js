function* counter(count) {
    while(count--) yield console.log(count)
}
for(const v of counter(5)) {
    console.log(v)
}
