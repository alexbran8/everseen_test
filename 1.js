fs = require('fs')

function fetchData() {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (Math.random() < 0.2) return reject(new Error('process error'))
            else return resolve({ fake: 'data' })
        }, 1000)
    })
}

function processData(data) {
    return new Promise((resolve, reject) => {
        console.log('processData', { data })
        setTimeout(() => {
            if (Math.random() < 0.2) return reject(new Error('process error'))
            else return resolve({ fake: 'processed' + data.fake })
        }, 1000)
    })
}

function process() {
    return new Promise((resolve, reject) =>
        fetchData()
            .then(data => {
                return processData(data)
                    .then((processedData) => resolve(processedData))
                    .catch((err) => reject(err))
            })
            .catch((err) => {
                console.log('process', { err: err?.toString() })
                return reject(err)
            })
    )
}

process()
    .then((data) => fs.writeFile('out.json', JSON.stringify(data), () => console.log('Done')))
    .catch((err) => console.error('Error processing data', err))