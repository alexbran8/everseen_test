// p = 10
const x = {
    p: 5,
    f: function () { return this.p }
}
const { f } = x
const y = {
    p: 20,
    f
}


console.log(f()) 
console.log(x.f())
console.log(y.f()) 