const authCheck = (req, res, next) =&gt; {
    if (!req.token) {
      res.status(401).json({
        authenticated: false,
        message: &quot;user has not been authenticated&quot; //could also implement token
  valability check
      });
    } else {
      next();
    }
  };
  
  app.use(&quot;/logparser&quot;, authCheck,  require(&quot;./controllers/logparser&quot;))