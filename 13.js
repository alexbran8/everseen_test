# Dockerfile
FROM node:15

# copy source and install dependencies
RUN mkdir -p /opt/app
COPY . /opt/app/

# build frontendk
WORKDIR /opt/app/client
RUN npm run dev

# backend
WORKDIR /opt/app/server
RUN npm install

# start server
EXPOSE 4000
STOPSIGNAL SIGTERM
CMD pm2-runtime start /opt/app/server/index.js -i max